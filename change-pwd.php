<?PHP
require_once("./include/sharepascher_config.php");

if(!$sharepascher->CheckLogin())
{
    $sharepascher->RedirectToURL("login.php");
    exit;
}

if(isset($_POST['submitted']))
{
   if($sharepascher->ChangePassword())
   {
        $sharepascher->RedirectToURL("changed-pwd.php");
   }
}

require_once('header.php');

?>
<section class="home-content">
  <div class="container">

    <div class="row title_block">
      <h2>Change Password</h2>
    </div>

    <form id='changepwd' action='<?php echo $sharepascher->GetSelfScript(); ?>' method='post' accept-charset='UTF-8'>

    <input type='hidden' name='submitted' id='submitted' value='1'/>

    <div class='short_explanation form-group'>
      <p>* Required fields</p>
    </div>

    <div class="form-group">
      <span class='error'><?php echo $sharepascher->GetErrorMessage(); ?></span>
    </div>
    <div class='form-group'>
        <label for='oldpwd' >Old Password *</label>
        <div class='pwdwidgetdiv' id='oldpwddiv' ></div><br/>
        <noscript>
        <input type='password' name='oldpwd' id='oldpwd' maxlength="50" class="form-control"/>
        </noscript>
        <span id='changepwd_oldpwd_errorloc' class='error'></span>
    </div>

    <div class='form-group'>
        <label for='newpwd' >New Password*:</label>
        <div class='pwdwidgetdiv' id='newpwddiv' ></div>
        <noscript>
        <input type='password' name='newpwd' id='newpwd' maxlength="50" class="form-control" /><br/>
        </noscript>
        <span id='changepwd_newpwd_errorloc' class='error'></span>
    </div>

    <br/><br/><br/>
    <div class='form-group'>
        <input type='submit' name='Submit' value='Submit' class="btn btn-primary"/>
    </div>

    </form>
  </div>
</section>


<script type='text/javascript'>
// <![CDATA[
    var pwdwidget = new PasswordWidget('oldpwddiv','oldpwd');
    pwdwidget.enableGenerate = false;
    pwdwidget.enableShowStrength=false;
    pwdwidget.enableShowStrengthStr =false;
    pwdwidget.MakePWDWidget();

    var pwdwidget = new PasswordWidget('newpwddiv','newpwd');
    pwdwidget.MakePWDWidget();


    var frmvalidator  = new Validator("changepwd");
    frmvalidator.EnableOnPageErrorDisplay();
    frmvalidator.EnableMsgsTogether();

    frmvalidator.addValidation("oldpwd","req","Please provide your old password");

    frmvalidator.addValidation("newpwd","req","Please provide your new password");

// ]]>
</script>

<?php require_once('footer.php'); ?>
