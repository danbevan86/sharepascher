<?php
  $pageTitle = "Home";
  session_start();
  if (!(isset($_SESSION['login']) && $_SESSION['login'] != '')) {
    header ("Location: index.php");
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
 <title>Share content</title>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
 <link rel="stylesheet" href="css/style.css" />

 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
 <script src="js/main.js"></script>
 <script>
   window.fbAsyncInit = function() {
     FB.init({
       appId      : '106344076478593',
       xfbml      : true,
       version    : 'v2.7'
     });
   };

   (function(d, s, id){
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {return;}
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/sdk.js";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
 </script>
</head>

<body>
  <div class="container">
    <form id="theform" action="<?php $_PHP_SELF ?>" method = "GET">
      <!-- <h2>Choose a social network.</h2>
      <input type="radio" value="facebook" id="facebookcheck" name="check" class="regular-radio" checked/>Facebook
      <input type="radio" value="twitter" id="twittercheck" name="check" class="regular-radio" />Twitter
      <input type="radio" value="linkedin" id="linkedincheck" name="check" class="regular-radio" />LinkedIn
      <input type="radio" value="linkedin" id="googlepluscheck" name="check" class="regular-radio"/>Google + -->
      <h2>Enter your content.</h2>
        <!-- <div class="charsleft" id="chars_fb"><strong id="fb_chars"></strong> <div style="display: inline" id="fb_chars_desc">characters</div> left</div>
        <div class="charsleft" id="chars_twit"><strong id="twit_chars"></strong> <div style="display: inline" id="twit_chars_desc">characters</div> left</div>
        <div class="charsleft" id="chars_linked"><strong id="linked_chars"></strong> <div style="display: inline" id="linked_chars_desc">characters</div> left</div> -->
      <div class="form-group">
        <label for="urlfield">Your URL</label>
        <input type="text" id="urlfield" class="form-control" />
      </div>
      <div class="form-group">
        <label for="titlefield">Title</label>
        <input type="text" id="titlefield" class="form-control" />
      </div>
      <div class="form-group">
        <label for="contentfield">Summary</label>
        <textarea class="form-control" id="contentfield" rows="4"></textarea>
      </div>
      <!-- <div class="fielddiv"><input type="text" id="imagefield" class="textfield" placeholder="Your custom image" /></div> -->
      <h2>Get the link.</h2>
      <div class="grad2" id="grad2">
       <button type="button" id="generate" class="btn btn-primary"><b>Generate URL</b></button>
      </div>
      <!-- <div class="lane" >
       <div id="fb_field" style="display:none">
       <div class="zocial icon facebook" style="font-size:11px;margin-bottom:-8px;">Facebook</div> <input type="text" id="fb_url" class="yoururl" value="" /> <button type="button" data-clipboard-text="" class="button white medium" id="copy_fb">copy</button>
       </div>
       </div> -->
       <div id="fb_field" class="form-group">
       <!-- <span class="zocial icon facebook" style="font-size:11px;margin-bottom:-8px;">Facebook</span> <input type="text" id="fb_url" class="yoururl" value="" /> <button type="button" data-clipboard-text="" class="button white medium" id="copy_fb">copy</button><br/><br/> -->
        <label for="fb_bitly">Facebook</label>
        <input type="text" id="fb_bitly" class="form-control yoururl" value="" />
        <button type="button" data-clipboard-text="" class="button white medium" id="copy_fb_bitly">Copy</button>
       </div>

       <div id="twit_field" class="form-group">
       <!-- <span class="zocial icon twitter" style="font-size:11px;margin-bottom:-8px;">Twitter</span> <input type="text" id="twit_url" class="yoururl" value="" /> <button type="button" data-clipboard-text="" class="button white medium" id="copy_twit">copy</button><br/><br/> -->
        <label for="twit_bitly">Twitter</label>
        <input type="text" id="twit_bitly" class="form-control yoururl" value="" />
        <button type="button" data-clipboard-text="" class="button white medium" id="copy_twit_bitly">Copy</button>
       </div>

       <div id="linked_field" class="form-group">
       <!-- <span class="zocial icon linkedin" style="font-size:11px;margin-bottom:-8px;">LinkedIn</span> <input type="text" id="linked_url" class="yoururl" value="" /> <button type="button" data-clipboard-text="" class="button white medium" id="copy_linked">copy</button><br/><br/> -->
        <label for="linked_bitly">LinkedIn</label>
        <input type="text" id="linked_bitly" class="form-control yoururl" value="" />
        <button type="button" data-clipboard-text="" class="button white medium" id="copy_bitly">Copy</button>
       </div>

       <div  id="gplus_field" class="form-group">
       <!-- <span class="zocial icon googleplus" style="font-size:11px;margin-bottom:-8px;">Google+</span> <input type="text" id="gplus_url" class="yoururl" value="" /> <button type="button" data-clipboard-text="" class="button white medium" id="copy_gplus">copy</button><br/><br/> -->
        <label for="linked_bitly">LinkedIn</label>
        <input type="text" id="gplus_bitly" class="form-control yoururl" value="" />
        <button type="button" data-clipboard-text="" class="button white medium" id="copy_gplus_bitly">Copy</button>
       </div>
      <!-- <div class="lane">

       </div> -->
    </form>
  </div>



<div id="footer">

</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.5.10/clipboard.min.js"></script>
<script>

(function(){
    new Clipboard('#copy_fb');
    new Clipboard('#copy_twit');
    new Clipboard('#copy_linked');
    new Clipboard('#copy_plus');
})();


</script>
<!-- <script src="js/ZeroClipboard.min.js"></script>
 <script src="js/clipboard.js"></script> -->

</body>
</html>
