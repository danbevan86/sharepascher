<?PHP
require_once("./include/sharepascher_config.php");

if(isset($_GET['code']))
{
   if($sharepascher->ConfirmUser())
   {
        $sharepascher->RedirectToURL("thank-you-regd.php");
   }
}

require_once('header.php');

?>

<section class="home-content">
  <div class="container">
    <div class="row title_block">
      <h2>Confirm registration</h2>
      <p>Please enter the confirmation code in the box below</p>
    </div>
    <div class="row">
      <form id="confirm" class="confirm" action="<?php echo $sharepascher->GetSelfScript(); ?>" method='get' accept-charset='UTF-8'>
        <div class='short_explanation'>* required fields</div>
        <div><span class='error'><?php echo $sharepascher->GetErrorMessage(); ?></span></div>
        <div class="form-group">
          <label for="code">Confirmation Code * </label>
          <input type="text" id="code" name="code" class="form-control" max-length="50"/>
          <span id='register_code_errorloc' class='error'></span>
        </div>

        <div class="form-group">
          <input type='submit' name='Submit' value='Submit' />
        </div>
      </form>
    </div>
  </div>
</section>

<script type='text/javascript'>
  var frmvalidator  = new Validator("confirm");
  frmvalidator.EnableOnPageErrorDisplay();
  frmvalidator.EnableMsgsTogether();
  frmvalidator.addValidation("code","req","Please enter the confirmation code");
</script>

<?php require_once('footer.php'); ?>
