<?PHP
  require_once("./include/sharepascher_config.php");
  require_once('header.php');
?>

<section class="home-content">
  <div class="container">

    <div class="row title_block">
      <h2>Reset password link sent</h2>
      <p>An email is sent to your email address that contains the link to reset the password.</p>
    </div>


  </div>
</section>

<?php
  require_once("footer.php");
  ?>
