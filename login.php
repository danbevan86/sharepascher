<?php

  require_once("./include/sharepascher_config.php");

  if(isset($_POST['submitted']))
  {
     if($sharepascher->Login())
     {
          $sharepascher->RedirectToURL("engage.php");
     }
  }

  require_once('header.php');
?>

<section class="home-content">
  <div class="container">
    <div class="row title_block">
      <h2>Login</h2>
      <p>Please submit your details to log in</p>
    </div>
    <div class="row">
      <div class="login-form">
        <div class="social-login">
          <h3>Login</h3>
          <p>Donec rutrum justo ac odio placerat congue. Aliquam ipsum lectus, iaculis id porta vitae, consequat congue justo. In eu tincidunt eros, non sodales tellus.</p>
          <div class="fb-login-button" data-max-rows="1" data-size="large" data-show-faces="false" data-auto-logout-link="false"></div>
        </div>
        <div class="share-login">
          <form id='login' action='<?php echo $sharepascher->GetSelfScript(); ?>' method='post' accept-charset='UTF-8'>

            <input type='hidden' name='submitted' id='submitted' value='1'/>

            <div class='short_explanation form-group'>
              <p>* Required fields</p>
            </div>

            <div class="form-group"><span class='error'><?php echo $sharepascher->GetErrorMessage(); ?></span></div>

            <div class="form-group">
              <label for="username">Username</label>
              <input type="text" name="username" id="username" value='<?php echo $sharepascher->SafeDisplay('username') ?>' maxlength="50" class="form-control">
              <span id='login_username_errorloc' class='error'></span>
            </div>
            <div class="form-group">
              <label for="password">Password</label>
              <input type="password" name="password" class="form-control" maxlength="15">
              <span id='login_password_errorloc' class='error'></span>
            </div>
            <input type='submit' name='Submit' value='Submit' class="btn btn-primary"/>

            <div class='short_explanation'><a href='reset-pwd-req.php'>Forgot Password?</a></div>
            <br><br>
            <p>If you wish to register a new account please click the button below</p>
            <a class="btn btn-primary" href="register.php">Sign up</a>
          </form>
        </div>
    </div>
    </div>
  </div>
</section>

<script type='text/javascript'>
// <![CDATA[

    var frmvalidator  = new Validator("login");
    frmvalidator.EnableOnPageErrorDisplay();
    frmvalidator.EnableMsgsTogether();

    frmvalidator.addValidation("username","req","Please provide your username");

    frmvalidator.addValidation("password","req","Please provide the password");

// ]]>
</script>


<?php require_once('footer.php'); ?>
