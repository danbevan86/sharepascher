<?php

require_once("./include/sharepascher_config.php");

require_once('header.php');
?>

  <section class="home-content">
    <div class="container">
      <div class="row title_block">
        <h2>SharePasCher</h2>
        <p>Create an engage to share on social media.</p>
        <a href="register.php" class="btn btn-primary home-signup">Sign up now</a>
      </div>
      <div class="row">
        <div class="col-md-4 col-sm-6">
          <div class="payment">
            <div class="payment-header trial">
              <h3>Trial</h3>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non diam pretium, ornare libero vel, cursus quam. Etiam vel tristique augue. Mauris fringilla mi vitae neque varius, a convallis tellus placerat. </p>
            </div>
            <div class="payment-features">
              <p>Fusce aliquam ipsum aliquam, facilisis sem in, congue lectus. Donec rutrum justo ac odio placerat congue.</p>
              <p>Fusce aliquam ipsum aliquam, facilisis sem in, congue lectus. Donec rutrum justo ac odio placerat congue.</p>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-6">
          <div class="payment">
            <div class="payment-header monthly">
              <h3>Monthly</h3>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non diam pretium, ornare libero vel, cursus quam. Etiam vel tristique augue. Mauris fringilla mi vitae neque varius, a convallis tellus placerat. </p>
            </div>
            <div class="payment-features">
              <p>Fusce aliquam ipsum aliquam, facilisis sem in, congue lectus. Donec rutrum justo ac odio placerat congue.</p>
              <p>Fusce aliquam ipsum aliquam, facilisis sem in, congue lectus. Donec rutrum justo ac odio placerat congue.</p>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-6">
          <div class="payment">
            <div class="payment-header annually">
              <h3>Annually</h3>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non diam pretium, ornare libero vel, cursus quam. Etiam vel tristique augue. Mauris fringilla mi vitae neque varius, a convallis tellus placerat. </p>
            </div>
            <div class="payment-features">
              <p>Fusce aliquam ipsum aliquam, facilisis sem in, congue lectus. Donec rutrum justo ac odio placerat congue.</p>
              <p>Fusce aliquam ipsum aliquam, facilisis sem in, congue lectus. Donec rutrum justo ac odio placerat congue.</p>
            </div>
          </div>
        </div>
    </div>
  </section>


  <?php require_once('footer.php'); ?>
