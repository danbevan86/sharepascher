<?PHP
require_once("./include/sharepascher_config.php");

$success = false;
if($sharepascher->ResetPassword())
{
    $success=true;
}

require_once('header.php');

?>
<section class="home-content">
  <div class="container">

    <?php
      if($success){
    ?>
    <div class="row title_block">
      <h2>Password is Reset Successfully</h2>
      <p>Your new password is sent to your email address.</p>
    </div>
    <?php
      } else {
    ?>
      <h2>Error</h2>
      <span class='error'><?php echo $sharepascher->GetErrorMessage(); ?></span>
    <?php } ?>

  </div>
</section>

<?php

require_once('footer.php');

?>
