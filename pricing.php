<?php

  require_once("./include/sharepascher_config.php");

  require_once('header.php');
?>

<section class="home-content">
  <div class="container">
    <div class="row title_block">
      <h2>Pricing</h2>
    </div>
    <div class="row">
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non diam pretium, ornare libero vel, cursus quam. Etiam vel tristique augue. Mauris fringilla mi vitae neque varius, a convallis tellus placerat. Fusce aliquam ipsum aliquam, facilisis sem in, congue lectus. Donec rutrum justo ac odio placerat congue. Aliquam ipsum lectus, iaculis id porta vitae, consequat congue justo. In eu tincidunt eros, non sodales tellus. Quisque blandit facilisis mi ut feugiat. Proin a turpis imperdiet ex dictum luctus. Nam quam neque, viverra vel semper vel, fringilla eget tellus. Curabitur pellentesque aliquam urna ut viverra. Proin nisl eros, auctor vitae sem at, elementum vestibulum libero. Proin vitae condimentum erat, non laoreet turpis. Nullam et orci vestibulum, volutpat ipsum id, dignissim ligula. Quisque elementum ut dolor in tincidunt. Aliquam dignissim dictum eros ac pharetra.</p>

    <p>Quisque vel tortor eget dolor porttitor aliquam. Pellentesque nec rutrum lorem. Phasellus aliquam erat velit, eget maximus mauris lobortis sed. Donec sodales dignissim velit vel eleifend. Morbi non nibh quis lacus ullamcorper maximus a non libero. Nam tellus mi, iaculis at porta a, accumsan id velit. Vivamus ut lorem hendrerit nunc lobortis facilisis. Vivamus mollis odio ac nibh accumsan, at efficitur libero porttitor. Mauris vitae est laoreet, vestibulum ante auctor, mollis nulla. Donec maximus ante ornare fringilla facilisis. Quisque nec tellus volutpat, dignissim neque et, tincidunt velit. Maecenas dapibus molestie pellentesque. Aliquam efficitur metus neque, vel dictum tellus tempus et. Nullam vulputate, nulla non molestie porttitor, eros turpis molestie mauris, eget congue eros enim in turpis.</p>
    </div>
  </div>
</section>
