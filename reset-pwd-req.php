<?PHP
require_once("./include/sharepascher_config.php");

$emailsent = false;
if(isset($_POST['submitted']))
{
   if($sharepascher->EmailResetPasswordLink())
   {
        $sharepascher->RedirectToURL("reset-pwd-link-sent.php");
        exit;
   }
}

require_once('header.php');

?>

<section class="home-content">
  <div class="container">

    <div class="row title_block">
      <h2>Reset Password</h2>
    </div>

    <div class="row">
      <form id='resetreq' action='<?php echo $sharepascher->GetSelfScript(); ?>' method='post' accept-charset='UTF-8'>


      <input type='hidden' name='submitted' id='submitted' value='1'/>

      <div class='short_explanation form-group'>
        <p>* Required fields</p>
      </div>

      <div class="form-group"><span class='error'><?php echo $sharepascher->GetErrorMessage(); ?></span></div>
      <div class='form-group'>
          <label for='email' >Your Email *</label><br/>
          <input type='text' name='email' id='email' value='<?php echo $sharepascher->SafeDisplay('email') ?>' maxlength="50" class="form-control"/><br/>
          <span id='resetreq_email_errorloc' class='error'></span>
      </div>
      <div class='short_explanation form-group'>
        <p>A link to reset your password will be sent to the email address</p>
      </div>
      <div class='form-group'>
          <input type='submit' name='Submit' value='Submit' class="btn btn-primary" />
      </div>

      </form>
    </div>

  </div>
</section>

<script type='text/javascript'>
// <![CDATA[

    var frmvalidator  = new Validator("resetreq");
    frmvalidator.EnableOnPageErrorDisplay();
    frmvalidator.EnableMsgsTogether();

    frmvalidator.addValidation("email","req","Please provide the email address used to sign-up");
    frmvalidator.addValidation("email","email","Please provide the email address used to sign-up");

// ]]>
</script>

</div>

<?php require_once('footer.php'); ?>
