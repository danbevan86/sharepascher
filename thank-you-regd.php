<?PHP
require_once('header.php');
?>

<section class="home-content">
  <div class="container">
    <div class="row title_block">
      <h2>Thanks for registering!</h2>
      <p>Your registration is now complete.</p>
    </div>
    <div class="form-group">
      <p><a href='login.php'>Click here to login</a></p>
    </div>
  </div>
</section>

<?php require_once('footer.php'); ?>
