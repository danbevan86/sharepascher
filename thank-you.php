<?PHP
require_once('header.php');
?>

<section class="home-content">
  <div class="container">
    <div class="row title_block">
      <h2>Thanks for registering!</h2>
      <p>Your confirmation email is on its way. Please click the link in the email to complete the registration.</p>
    </div>
  </div>
</section>

<?php require_once('footer.php'); ?>
