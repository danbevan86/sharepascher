<?php

require_once('header.php');

?>

<section class="home-content">
  <div class="container">

    <div class="row title_block">
      <h2>Changed password</h2>
      <p>Your password is updated!</p><br/><br/>
      <p><a href='logout.php'>logout</a></p>
    </div>

  </div>

</section>

<?php

require_once('footer.php');

?>
