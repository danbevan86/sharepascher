<?php

require_once("./include/sharepascher_config.php");

if(isset($_POST['submitted']))
{
   if($sharepascher->RegisterUser())
   {
        $sharepascher->RedirectToURL("thank-you.php");
   }
}
  require_once('header.php');

?>

  <section class="home-content">
    <div class="container">
      <div class="row title_block">
        <h2>Register</h2>
        <p>Please complete the form to create an account</p>
      </div>
      <div class="row">
        <form id='register' action='<?php echo $sharepascher->GetSelfScript(); ?>' method='post' accept-charset='UTF-8'>
          <input type="hidden" name="submitted" id="submitted" value="1" />

          <div class='short_explanation form-group'>
            <p>* Required fields</p>
          </div>
          <input type='text'  class='spmhidip' name='<?php echo $sharepascher->GetSpamTrapInputName(); ?>' />

      <div><span class='error'><?php echo $sharepascher->GetErrorMessage(); ?></span></div>
      <div class="form-group">
        <label for="name">Name *</label>
        <input type="text" name="name" id="name" value='<?php echo $sharepascher->SafeDisplay('name') ?>' class="form-control" maxlength="50" />
        <span id='register_name_errorloc' class='error'></span>
      </div>
      <div class="form-group">
        <label for="company">Company *</label>
        <input type="text" name="company" id="company" value='<?php echo $sharepascher->SafeDisplay('company') ?>' class="form-control" maxlength="50" >
        <span id='register_company_errorloc' class='error'></span>
      </div>
      <div class="form-group">
        <label for="company">E-mail address *</label>
        <input type="email" name="email" id="email" value='<?php echo $sharepascher->SafeDisplay('email') ?>' class="form-control" maxlength="40">
        <span id='register_email_errorloc' class='error'></span>
      </div>
      <div class="form-group">
        <label for="username">Username *</label>
        <input type="text" name="username" id="username" value='<?php echo $sharepascher->SafeDisplay('username') ?>' class="form-control" maxlength="50" >
        <span id='register_username_errorloc' class='error'></span>
      </div>
      <div class="form-group">
        <label for="password">Password *</label>
        <div class='pwdwidgetdiv' id='thepwddiv' ></div>
        <noscript>
        <input type='password' name='password' id='password' class="form-control" />
        </noscript>
        <div id='register_password_errorloc' class='error' style='clear:both'></div>
      </div>
      <div class="form-group">
        <input type='submit' name='Submit' value='Submit' class="btn btn-primary" />
        <div id="submit">
        </div>
      </div>
      <!-- <div id="wrongpw">Password incorrect, please try again</div> -->
    </form>
  </div>

  </div>
</section>

<script>

  var pwdwidget = new PasswordWidget('thepwddiv','password');
  pwdwidget.MakePWDWidget();

  var frmvalidator  = new Validator("register");
  frmvalidator.EnableOnPageErrorDisplay();
  frmvalidator.EnableMsgsTogether();
  frmvalidator.addValidation("name","req","Please provide your name");
  frmvalidator.addValidation("company","req","Please provide your company");
  frmvalidator.addValidation("email","req","Please provide your email address");
  frmvalidator.addValidation("email","email","Please provide a valid email address");
  frmvalidator.addValidation("username","req","Please provide your name");
  frmvalidator.addValidation("password","req","Please provide a password");
</script>

<?php require_once('footer.php'); ?>
