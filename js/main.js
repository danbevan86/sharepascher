var twitterUrlFound = false;
var linked_bit;
var fb_bit;
var twit_bit;
var gplus_bit;

$(document).ready (function() {

  // init();
  // resetChars();


  $('#tweetfield').keyup(function(){
    init();
    $('#chars_twit').show();
  })

  $('.more-info').tooltip({
    effect: "fade",
    opacity: 0.7
  });

  $('#generate').click (function() {

    // $('#urlfield').attr('class','textfield');

    url = $('#urlfield').val();
    title = $('#titlefield').val();
    content = $('#contentfield').val();
    tweet = $('#tweetfield').val();
    retweet = $('#retweeturl').val();

    if (url == '' || !ValidUrl(url)) {
      $('.user-url').addClass('alert alert-danger');
      $('.user-url .error').show();
      return false;
    }

    bitly(url);

    })
})

window.onload = function() {
  if (top.location.pathname === '/reporting.php') {

    $('.engagebitly').each(function(i) {
      engageLink = $(this).attr("href");
      getClicks(engageLink, this);
    });

    $('.fblink').each(function(i){
      fbLink = $(this).attr("href");
      getClicks(fbLink, this);
    });

    $('.twitlink').each(function(i){
      twitLink = $(this).attr("href");
      getClicks(twitLink, this);
    });

    $('.linkedinlink').each(function(i){
      linkedLink = $(this).attr("href");
      getClicks(linkedLink, this);
    });

    $('.gpluslink').each(function(i){
      gplusLink = $(this).attr("href");
      getClicks(gplusLink, this);
    });

    function getClicks(url, bitlyclass) {

      var username = "danbevan"; // bit.ly username
      var key = "R_fd5d31027fbb4412aafc7383a923bfde";
      var clks;
      $.ajax({
        url:"http://api.bit.ly/v3/clicks",
        data:{shortUrl:url,apiKey:key,login:username},
        dataType:"jsonp",
        success:function(v)
        {
          clks=v.data.clicks[0].user_clicks;
          $(bitlyclass).next('span').html("(" + clks + ")");
          clickcount = clks;
          return clickcount;
        }
      });
      console.log(clickcount);
    }
  }
};

function init () {

  var twit_chars = 140 - $('#tweetfield').val().length;
  var twit_chars = 140 - calculateTwitterChars ();

  if ( twit_chars == -1 || twit_chars == 1 )
    $('#twit_chars_desc').html('character');
  else
    $('#twit_chars_desc').html('characters');

  if ( twit_chars < 0 ) twit_chars = '<span class="minus">' + twit_chars + '</span>';

  $('#twit_chars').html(twit_chars);

}

function calculateTwitterChars () {
    var textLen = 0;
    var text = $('#tweetfield').val();
    if ( text == '') return textLen;

    var words = text.split(' ');

    twitterUrlFound = false;

    for (i=0;i<=words.length-1;i++) {
        var wordLen;
        //if ( (words[i] + '').indexOf('http://', 0) ) wordLen = 22;
        //else if ( (words[i] + '').indexOf('https://', 0) ) wordLen = 23;
        //else
        if ( (words[i]).indexOf('http://', 0) != -1 && !twitterUrlFound ) { wordLen = 22; twitterUrlFound = true; }
        else if ( (words[i]).indexOf('https://', 0) != -1 && !twitterUrlFound ) { wordLen = 23; twitterUrlFound = true; }
        else wordLen = words[i].length;

        textLen = textLen + wordLen + 1;
    }

    return textLen-1;
}

function ValidUrl(str) {
  var pattern = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;

  if(!pattern.test(str)) {
    return false;
  } else {
    return true;
  }
}

var orig_link = $('#bitly-url').attr("href");

function resetChars() {
  $('#chars_twit').hide();
}

function bitly(url) {

  var username = "danbevan"; // bit.ly username
  var key = "R_fd5d31027fbb4412aafc7383a923bfde";
  $.ajax({
    url:"http://api.bit.ly/v3/shorten",
    data:{longUrl:url,apiKey:key,login:username},
    dataType:"jsonp",
    success:function(v)
    {
      bitly_url=v.data.url;
      twit_link = "https://twitter.com/intent/tweet?text=" + encodeURIComponent(tweet) + " " + bitly_url;
      fb_link = "https://www.facebook.com/dialog/feed?app_id=106344076478593&link=" + bitly_url + "&name=" + encodeURI(title) + "&description=" + encodeURIComponent(content);
      linkedin_link = "https://www.linkedin.com/shareArticle?mini=true&url=" + bitly_url + "&title=" + encodeURI(title) +  "&summary=" + encodeURIComponent(content);
      google_link = "https://plus.google.com/share?url=" + bitly_url;

      bit_url_fb(fb_link);
      bit_url_linked(linkedin_link);
      bit_url_google(google_link);
      bit_url_twit(twit_link);
      bit_url_retweet(retweet);

      $('#status_text').html("<b>Bitly:</b> <a target='_blank' href=" + bitly_url + ">" + bitly_url + "</a><br/><br/><b>Title:</b> " + title + "<br/><br/><b>Content:</b> " + content + "<br/><br/><b>Tweet:</b> " + tweet + " <a target='_blank' href=" + bitly_url + ">" + bitly_url + "</a>");
      $('.bitly_output').show();
      $('.eml2').show();
      $('#bitly__url').html(bitly_url);
      emlbody = "I saw this and thought you'd like to read it: " + bitly_url;
      $('#eml_bitly').attr("href", "http://mailto:?subject=" + title + "&body=" + emlbody);
      email1 =   $('#eml_bitly').attr("href");
      bit_url_email(email1);
      var eml2_subject = "Please share this " + title;
      $('#eml_bitly2').attr("href", "mailto:?subject=" + eml2_subject + "&body=" + $('.bitly_output').html());

    }
  });

}

function bit_url_fb(url) {
  var username = "danbevan"; // bit.ly username
  var key = "R_fd5d31027fbb4412aafc7383a923bfde";
  $.ajax({
    url:"http://api.bit.ly/v3/shorten",
    data:{longUrl:url,apiKey:key,login:username},
    dataType:"jsonp",
    success:function(v)
    {
      $('#fb_bitly').attr("href", v.data.url);
      fb_bit = $('#fb_bitly').attr("href");
    }
  });
}

function bit_url_linked(url) {
  var username = "danbevan"; // bit.ly username
  var key = "R_fd5d31027fbb4412aafc7383a923bfde";
  $.ajax({
    url:"http://api.bit.ly/v3/shorten",
    data:{longUrl:url,apiKey:key,login:username},
    dataType:"jsonp",
    success:function(v)
    {
      $('#linked_bitly').attr("href", v.data.url);
      linked_bit = $('#linked_bitly').attr("href");
    },
    complete: function (v) {
      saveEngage();
    }
  });
}

function bit_url_twit(url) {
  var username = "danbevan"; // bit.ly username
  var key = "R_fd5d31027fbb4412aafc7383a923bfde";
  $.ajax({
    url:"http://api.bit.ly/v3/shorten",
    data:{longUrl:url,apiKey:key,login:username},
    dataType:"jsonp",
    success:function(v)
    {
      $('#twit_bitly').attr("href", v.data.url);
      twit_bit = $('#twit_bitly').attr("href");
    }
  });
}


function bit_url_google(url) {
  var username = "danbevan"; // bit.ly username
  var key = "R_fd5d31027fbb4412aafc7383a923bfde";
  $.ajax({
    url:"http://api.bit.ly/v3/shorten",
    data:{longUrl:url,apiKey:key,login:username},
    dataType:"jsonp",
    success:function(v)
    {
      $('#gplus_bitly').attr("href", v.data.url);
      gplus_bit = $('#gplus_bitly').attr("href");
    }
  });
}

function bit_url_retweet(url) {
  var username = "danbevan"; // bit.ly username
  var key = "R_fd5d31027fbb4412aafc7383a923bfde";
  $.ajax({
    url:"http://api.bit.ly/v3/shorten",
    data:{longUrl:url,apiKey:key,login:username},
    dataType:"jsonp",
    success:function(v)
    {
      $('#retweet_bitly').attr("href", v.data.url);
      retweet_bit = $('#retweet_bitly').attr("href");
    }
  });
}

function bit_url_email(url) {
  var username = "danbevan"; // bit.ly username
  var key = "R_fd5d31027fbb4412aafc7383a923bfde";
  $.ajax({
    url:"http://api.bit.ly/v3/shorten",
    data:{longUrl:url,apiKey:key,login:username},
    dataType:"jsonp",
    success:function(v)
    {
      $('#eml_bitly').attr("href", v.data.url);
      eml_bitly = $('#eml_bitly').attr("href");
    }
  });
}

function saveEngage () {

  console.log(bitly_url);
  //make the postdata
  var postData = 'engageURL='+bitly_url+'&engageTitle='+title+'&engageContent='+content+'&engageTweet='+tweet+'&fbLink='+fb_bit+'&linkedinLink='+linked_bit+'&twitLink='+twit_bit+'&gplusLink='+gplus_bit;
  $.ajax({
   url : "include/sharepascher.php?action=saveEngage",
   type: "POST",
   data : postData,
   success: function(data,status, xhr)
   {
     console.log(postData);
       //if success then just output the text to the status div then clear the form inputs to prepare for new data
       $("#status_text").html(data);

       $('#urlfield').val('');
       $('#titlefield').val('');
       $('#contentfield').val('');
       $('#tweetfield').val('');
   },
   error: function (jqXHR, status, errorThrown)
   {
       //if fail show error and server status
        $("#status_text").html('there was an error ' + errorThrown + ' with status ' + textStatus);
   }
 });
}

function saveClicks () {

  //make the postdata
  var postData = 'bitlyClicks='+bitly_url+'&fbClicks='+title+'&twitClicks='+content+'&linkedClicks='+tweet+'&gplusClicks='+fb_bit;
  $.ajax({
   url : "saveClicks.php",
   type: "POST",
   data : postData,
   success: function(data,status, xhr)
   {
     console.log(postData);
   },
   error: function (jqXHR, status, errorThrown)
   {
       //if fail show error and server status
   }
 });
}
