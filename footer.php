<footer class="footer">
  <div class="container">
    <div class="row">
      <div class="col-md-3">
        <h3>About</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus aliquet libero at metus suscipit imperdiet. Curabitur est mi, consectetur quis mi at, vulputate lobortis nisl. Mauris vel ullamcorper quam.</p>
      </div>
      <div class="col-md-3">
        <h3>Social Media</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus aliquet libero at metus suscipit imperdiet. Curabitur est mi, consectetur quis mi at, vulputate lobortis nisl. Mauris vel ullamcorper quam.</p>
      </div>
      <div class="col-md-3">
        <h3>Contact</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus aliquet libero at metus suscipit imperdiet. Curabitur est mi, consectetur quis mi at, vulputate lobortis nisl. Mauris vel ullamcorper quam.</p>
      </div>
      <div class="col-md-3">
        <h3>About</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus aliquet libero at metus suscipit imperdiet. Curabitur est mi, consectetur quis mi at, vulputate lobortis nisl. Mauris vel ullamcorper quam.</p>
      </div>
    </div>
  </div>
</footer>
<div class="copyright">
  <p>SharePasCher &copy; 2016 All Rights Reserved.</p>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.5.10/clipboard.min.js"></script>

</body>
</html>
