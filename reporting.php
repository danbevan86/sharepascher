<?php

require_once("./include/sharepascher_config.php");

if(!$sharepascher->CheckLogin())
{
    $sharepascher->RedirectToURL("login.php");
    exit;
}

require_once('header.php');

?>
<section class="home-content">
  <div class="container">
    <div class="row title_block">
      <h2>Shared Links</h2>
      <p>Please find the list of engages you have shared, for each URL created, it will have the number of times it has been clicked in brackets.</p>
    </div>
    <div class="row">
      <?php $sharepascher->getReports(); ?>
    </div>
  </div>
</section>
<?php require_once('footer.php'); ?>
