<!DOCTYPE html>
<html lang="en">
<head>
 <title>Share content</title>
 <link rel="stylesheet" href="css/bootstrap.min.css">
 <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
 <link rel="stylesheet" href="css/style.css" />
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
 <script src="js/gen_validatorv4.js" type="text/javascript"></script>
 <script src="js/pwdwidget.js" type="text/javascript"></script>
 <script src="js/bootstrap.min.js"></script>
 <script src="js/main.js"></script>
 <script>
   window.fbAsyncInit = function() {
     FB.init({
       appId      : '106344076478593',
       xfbml      : true,
       version    : 'v2.7'
     });
   };

   (function(d, s, id){
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {return;}
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/sdk.js";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
 </script>
</head>
<body>
  <header>
    <div class="container">
      <div class="logo col-xs-12 col-sm-3">
        <a href="/"><h1>SharePasCher</h1></a>
      </div>
      <div class="header-right col-xs-12 col-sm-9">
        <nav class="navbar navbar-default col-xs-12 col-sm-12 col-md-10">
          <div class="container-fluid">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                <li><a href="/">Home</a></li>
                <?php if($sharepascher->CheckLogin()) { ?>
                <li><a href="engage.php">Share</a></li>
                <?php } ?>
                <li><a href="pricing.php">Pricing</a></li>
                <?php if($sharepascher->CheckLogin()) { ?>
                <li><a href="/reporting.php">Reporting</a></li>
                <li><a href="/change-pwd.php">Account</a></li>
                <?php } ?>
                <li><a href="/contact.php">Contact</a></li>
                <?php if($sharepascher->CheckLogin()) { ?>
                <li><a href="logout.php?logout">Log out</a></li>
                <?php } else { ?>
                <li><a href="login.php">Login</a></li>
                <li><a href="register.php">Register</a></li>
                <?php } ?>
              </ul>
            </div><!--/.nav-collapse -->
          </div><!--/.container-fluid -->
        </nav>
      </div>
    </div>
  </header>
