<?php

require_once("./include/sharepascher_config.php");

if(!$sharepascher->CheckLogin())
{
    $sharepascher->RedirectToURL("login.php");
    exit;
}

require_once('header.php');
?>

  <section class="home-content">
    <div class="container">
      <div class="row title_block">
        <h2>Create your Engage</h2>
        <p>Please fill in the form below to create an engage to share on social media.</p>
      </div>
      <div class="row">
        <form id="theform" class="theform" onsubmit="return false">
          <div class="form-group user-url">
            <label for="urlfield">Your URL <span>(http://www.website.co.uk)</span></label>
            <input type="text" id="urlfield" name="engageURL" class="form-control"/>
            <span id='urlfield_errorloc' class='error'></span>
          </div>
          <div class="form-group user-title">
            <label for="titlefield">Title</label>
            <input type="text" id="titlefield" name="title" class="form-control" />
            <span id='titlefield_errorloc' class='error'></span>
          </div>
          <div class="form-group user-summary">
            <label for="contentfield">Summary</label>
            <textarea class="form-control" id="contentfield" name="content" rows="4"></textarea>
          </div>
          <div class="form-group user-tweet">
            <label for="tweetfield">Tweet
            <span class="charsleft" id="chars_twit"><strong id="twit_chars"></strong> <div style="display: inline" id="twit_chars_desc">characters</div> left</label>
            <textarea class="form-control" id="tweetfield" name="tweet" rows="4"></textarea>
          </div>

          <p id="bitly__url" style="display: none;"></p>

          <!-- <div class="fielddiv"><input type="text" id="imagefield" class="textfield" placeholder="Your custom image" /></div> -->
          <div class="grad2" id="grad2">
            <button type="button" id="generate" class="btn btn-primary"><b>Generate URL</b></button>
          </div>
        </form>


        <div id="status_text"></div>

        <p class="bitly_output" style="display: none;">Share this on <a id="fb_bitly" target="_blank">Facebook</a>, <a id="linked_bitly" target="_blank">LinkedIn</a>, <a id="twit_bitly" target="_blank">Twitter</a>, <a id="gplus_bitly" target="_blank">Google+</a> or <a id="eml_bitly">Email</a></p>
        <p class="eml2" style="display: none;"><b><a id="eml_bitly2">Email to share links</a></b></p>
     </div>
    </div>
  </section>

  <script>

    var frmvalidator  = new Validator("theform");
    frmvalidator.EnableOnPageErrorDisplay();
    frmvalidator.EnableMsgsTogether();
    frmvalidator.addValidation("engageURL","req","Please provide a URL");
    frmvalidator.addValidation("title","req","Please provide a name");
  </script>

  <?php require_once('footer.php'); ?>
