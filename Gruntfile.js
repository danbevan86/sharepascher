module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    sass: {
			dist: {
				files: {
					'css/style.css' : 'sass/style.scss'
				}
			}
		},
    watch: {
      css: {
				files: '**/*.scss',
				tasks: ['sass']
			}
    }
  });

  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.registerTask('test', ['watch', 'sass']);

  grunt.registerTask('default', ['watch', 'sass']);

};
